import './App.css';
import SettingsPanel from './components/SettingsPanel';
import DeckPicked from './components/DeckPicked';
import Deck from './components/Deck';
import History from './components/History';
import Notification from './components/Notification';
import Stats from './components/Stats';

function App() {

  return (
    <main className='main'>
      <section className='decks'>
        <DeckPicked />
        <Deck />
        <div className='deck'>
          <History />
          <Stats />
        </div>
      </section>
      <SettingsPanel />
      <Notification />
    </main>
  );
}

export default App;
