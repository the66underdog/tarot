import { createAction, createReducer } from "@reduxjs/toolkit";

const initState = { status: 0 };

export const setNotification = createAction('SET_NOTIFICATION');
export const hideNotification = createAction('HIDE_NOTIFICATION');

export default createReducer(initState, {
    [setNotification]: (_, action) => {
        return {
            status: action.payload.status,
            message: action.payload.message,
            timer: action.payload.timer * 1000,
        }
    },
    [hideNotification]: (prev) => {
        return {
            status: 0,
            message: prev.message,
        }
    }
});