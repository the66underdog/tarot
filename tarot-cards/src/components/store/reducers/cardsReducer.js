import { createAction, createReducer } from "@reduxjs/toolkit";

const initState = {
    amount: 10, cards: [], reducer: 1, stats: {},
};

export const genCard = createAction('GEN_CARD');
export const toggleInf = createAction('MAKE_INFINITE');
export const rstStats = createAction('RESET_STATS');
export const setAmount = createAction('SET_AMOUNT');

export default createReducer(initState, {
    [genCard]: (prev, action) => {
        prev.amount = prev.amount - prev.reducer;
        prev.cards = [...prev.cards, action.payload];
        prev.stats[action.payload.name] = prev.stats[action.payload.name] ?
            ++prev.stats[action.payload.name] : 1;
    },
    [toggleInf]: prev => {
        return { ...prev, amount: 1, reducer: prev.reducer ? 0 : 1 };
    },
    [setAmount]: (prev, action) => {
        return { ...prev, amount: action.payload }
    },
    [rstStats]: prev => {
        return { ...prev, cards: [], stats: {} };
    },
});