import { combineReducers, configureStore } from '@reduxjs/toolkit';
import cardsReducer from './reducers/cardsReducer';
import notificationReducer from './reducers/notificationReducer';

const rootReducer = combineReducers({
    cards: cardsReducer,
    notification: notificationReducer,
})

export default configureStore({
    reducer: rootReducer,
})