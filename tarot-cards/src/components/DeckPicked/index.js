import { useSelector } from 'react-redux';
import Picked from '../Picked';
import Stacked from '../Stacked';

function DeckPicked() {

    const cards = useSelector(state => state.cards.cards);

    const preLastCard = cards[cards.length - 2];
    const lastCard = cards[cards.length - 1];


    return (
        <div className='deck decks__item-picked'>
            {cards.length > 1 && <Stacked cardType={preLastCard} />}
            {cards.length !== 0 && <Picked key={cards.length} cardType={lastCard} />}
        </div>
    );
}

export default DeckPicked;