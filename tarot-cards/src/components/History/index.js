import { useSelector } from 'react-redux';
import './style.css';

function History() {

    const deck = useSelector(state => state.cards.cards);

    function parseCardName(cardName) {
        const article = 'the';
        switch (cardName) {
            case 'wof':
                return `${article} wheel of fortune`;
            case 'preistess':
                return `${article} high preistess`;
            case 'hanged':
                return `${article} hanged man`;
            case 'death':
                return cardName
            default:
                return `${article} ${cardName}`;
        }
    }

    function colorHistoryItem(e) {
        if (e.target.closest('.history__item')) {
            e.target.closest('.history__item').firstElementChild.classList.remove('history__label_base-color');
        }
    }

    function decolorHistoryItem(e) {
        if (e.relatedTarget) {
            if (!e.target.closest('.history__label')) {
                if (e.target.closest('.history__item') && !e.relatedTarget.closest('.history__label')) {
                    e.target.firstElementChild.classList.add('history__label_base-color');
                }
            }
        }
    }

    function handleHistoryItemAnimation(e) {
        e.currentTarget.removeEventListener('animationend', handleHistoryItemAnimation);
        e.currentTarget.lastElementChild.style.opacity = 1;
    }

    return (
        <ul className='history' onMouseOver={e => colorHistoryItem(e)}
            onMouseOut={e => decolorHistoryItem(e)}>
            {deck.map((_, index) => {
                return <li key={deck.length - 1 - index} className='history__item'
                    onAnimationEnd={e => handleHistoryItemAnimation(e)}>
                    <span className='history__label history__label_base-color' style={{ color: deck[deck.length - 1 - index].color }}>
                        {parseCardName(deck[deck.length - 1 - index].name)}
                    </span>
                </li>;
            })}
        </ul>
    );
}

export default History;