import { useState } from "react";
import Desc from "../Desc";
import './style.css';
import back from '../../asset/images/back.png';
import cardTypes from '../Types';

function Picked({ cardType }) {

    const [desc, setDesc] = useState(0);

    function handleAnimationEndSideEffects(e) {
        e.currentTarget.style.cssText = `
            border: 3px solid ${cardType.color};
            --color-card-type: ${cardType.color};
            animation: shineEffect var(--time-shine-effect) infinite;
            pointer-events: all;
        `;
        // Remove 2nd layer card to show the fool card
        if (cardType.name === 'fool') {
            e.currentTarget.lastElementChild.style.animation = 'fadeAway var(--time-standard-delay) ease-out';
        }
    }

    function showFool() {
        const name = generateNotFool();
        return <img className="deck__card deck__card_reversed"
            src={cardTypes[name].url} alt={`${name}`}
            onAnimationEnd={e => { e.currentTarget.style.cssText = `visibility: hidden; opacity: 0;` }} />;
    }

    function generateNotFool() {
        const types = ['tower', 'wof', 'devil', 'death', 'hermit', 'sun', 'moon', 'preistess', 'hanged'];
        return types[Math.round(Math.random() * (types.length - 1))];
    }

    function hideDescription(e) {
        e.relatedTarget instanceof Element &&
            !e.relatedTarget.closest('.description') &&
            setDesc(0);
    }

    return (
        <>
            <div className='deck__card-container deck__card-container_picked deck__card-container_moved'
                onAnimationEnd={e => { handleAnimationEndSideEffects(e); }}
                onMouseOver={() => setDesc(1)}
                onMouseOut={e => hideDescription(e)}>
                <img className="deck__card" alt='Card back'
                    src={back} />
                <img className="deck__card deck__card_reversed" alt={cardType.name}
                    src={cardType.url} />
                {cardType.name === 'fool' && showFool()}
            </div>
            <Desc cardName={cardType.name} setDesc={setDesc} isDesc={desc} />
        </>
    );
}

export default Picked;