import { useDispatch, useSelector } from 'react-redux';
import './style.css';
import { toggleInf, resetStats, setAmount } from '../store/reducers/cardsReducer';

function SettingsPanel() {

    const dispatch = useDispatch();
    const reducer = useSelector(state => state.cards.reducer);

    function setDeckAmount(e) {
        e.preventDefault();
        const formData = new FormData(e.currentTarget);
        dispatch(setAmount(formData.get('amount')));
    }

    return (
        <form className='settings-panel' onSubmit={e => setDeckAmount(e)}>
            <label className='input-checkbox settings-panel__input-checkbox' htmlFor='infinite'>
                <span>Infinite</span>
                <input onChange={() => dispatch(toggleInf())} type='checkbox' className='input-checkbox__box' id='infinite' />
            </label>
            <input type='text' name='amount' className='input-text settings-panel__input-text' placeholder='666'
                disabled={reducer ? false : true} />
            <button type='submit'
                className='button settings-panel__button-refresh'
                disabled={reducer ? false : true}>
                <span>Refresh deck</span>
            </button>
            <button
                type='button'
                className='button settings-panel__button-reset'
                disabled={reducer ? false : true}
                onClick={() => dispatch(resetStats())}>
                <span>Reset stats</span>
            </button>
        </form>
    );
}

export default SettingsPanel;