import wof from '../asset/images/wof.png';
import tower from '../asset/images/tower.png';
import fool from '../asset/images/fool.png';
import devil from '../asset/images/devil.png';
import death from '../asset/images/death.png';
import hermit from '../asset/images/hermit.png';
import sun from '../asset/images/sun.png';
import moon from '../asset/images/moon.png';
import preistess from '../asset/images/preistess.png';
import hanged from '../asset/images/hanged.png';




const cards = {
    tower: {
        name: 'tower',
        color: '#0080ee',
        url: tower,
    },
    wof:
    {
        name: 'wof',
        color: () => (Math.random() < 0.5 ? '#00dd00' : '#ff4714'),
        url: wof,
    },
    fool:
    {
        name: 'fool',
        color: '#ba48e0',
        url: fool,
    },
    devil:
    {
        name: 'devil',
        color: '#f25277',
        url: devil,
    },
    death:
    {
        name: 'death',
        color: '#8400c3',
        url: death,
    },
    hermit:
    {
        name: 'hermit',
        color: '#53e1e6',
        url: hermit,
    },
    sun:
    {
        name: 'sun',
        color: '#ffd700',
        url: sun,
    },
    moon:
    {
        name: 'moon',
        color: '#cccccc',
        url: moon,
    },
    preistess:
    {
        name: 'preistess',
        color: '#fce883',
        url: preistess,
    },
    hanged:
    {
        name: 'hanged',
        color: 'rgb(66, 66, 66)',
        url: hanged,
    },
}

export default cards;