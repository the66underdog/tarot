import { useDispatch } from 'react-redux';
import { genCard } from '../store/reducers/cardsReducer';
import Cards from '../Types';
import back from '../../asset/images/back.png';

function Card() {

    const dispatch = useDispatch();

    function generateCardType() {
        const chance = Math.random();
        switch (true) {
            case (chance < 0.2):
                return Cards.tower;
            case (chance < 0.4):
                return { ...Cards.wof, color: Cards.wof.color() };
            case (chance < 0.57):
                return Cards.fool;
            case (chance < 0.67):
                return Cards.devil;
            case (chance < 0.77):
                return Cards.death;
            case (chance < 0.87):
                return Cards.hermit;
            case (chance < 0.92):
                return Cards.sun;
            case (chance < 0.97):
                return Cards.moon;
            case (chance < 0.99):
                return Cards.preistess;
            default:
                return Cards.hanged;
        }
    }

    function pickCard(e) {
        if (e.target.closest('.deck__card-container')) {
            dispatch(genCard(generateCardType()));
        }
    }

    return (
        <div className='deck__card-container' onClick={e => { pickCard(e) }}>
            <img className='deck__card' src={back} alt='Card back' />
        </div>
    );
}

export default Card;