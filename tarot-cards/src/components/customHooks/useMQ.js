import { useState } from "react";

export default function useMQ(strMQ) {

    const mediaQuery = window.matchMedia(strMQ);
    const [matches, setMatches] = useState(mediaQuery.matches);

    function toggleMQ() {
        setMatches(mediaQuery.matches);
    }

    mediaQuery.addEventListener('change', toggleMQ);

    return matches;
}