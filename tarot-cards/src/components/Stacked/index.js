function Stacked({ cardType }) {

    return (
        <div className='deck__card-container' style={{ border: `3px solid ${cardType.color}` }}>
            <img className="deck__card" src={cardType.url} alt={cardType.name} />
        </div>
    );
}

export default Stacked;