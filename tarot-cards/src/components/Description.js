export const CardDescription = {
    tower: 'Causes an interaction and doubles all ghost activity for 20 seconds.',
    wof: 'The player gains 25% sanity if the card burns green, or loses 25% sanity if the card burns red.',
    fool: 'Will appear as another card at first, before turning into The Fool as it burns. No effect is applied when drawing this card.',
    devil: 'Triggers a Ghost Event towards the nearest player to the ghost.',
    death: 'Triggers a cursed hunt.',
    hermit: 'Teleports the ghost back to its ghost room and traps it there for 1 minute. This does not affect hunts.',
    sun: 'Fully restores sanity to 100%.',
    moon: 'Instantly drops sanity to 0%.',
    preistess: 'Revives a randomly chosen dead player at their corpse. If no one is currently dead, it will revive the next player who dies. Multiple cards do not stack. If a player dies during a hunt and is revived, they cannot be killed again in the same hunt.',
    hanged: 'Instantly kills the player. This card will always turn into The Fool if the "Friendly Ghost" custom difficulty option is turned on.',
}