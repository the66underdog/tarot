import { useDispatch, useSelector } from 'react-redux';
import './style.css';
import { toggleInf, rstStats, setAmount } from '../store/reducers/cardsReducer';
import { setNotification } from '../store/reducers/notificationReducer';

function SettingsPanel() {
    const dispatch = useDispatch();
    const reducer = useSelector(state => state.cards.reducer);

    function setDeckAmount(e) {
        e.preventDefault();
        const formData = new FormData(e.currentTarget);
        if (/\D/.test(`${formData.get('amount')}`.trim())) {
            dispatch(setNotification({
                status: 'err',
                message: 'Amount input accepts numbers only',
                timer: 3,
            }));
        }
        else {
            dispatch(setNotification({
                status: 'fine',
                message: 'Deck amount has been set',
                timer: 3,
            }));
            dispatch(setAmount(formData.get('amount')));
        }
    }

    function toggleInfiniteAmount() {
        reducer ?
            dispatch(setNotification({
                status: 'info',
                message: 'Deck amount is now INFINITE',
                timer: 3,
            })) :
            dispatch(setNotification({
                status: 'info',
                message: 'Deck amount is now LIMITED',
                timer: 3,
            }));
        dispatch(toggleInf());
    }

    function resetStats() {
        dispatch(setNotification({
            status: 'info',
            message: 'Statistic has been reset',
            timer: 3,
        }));
        dispatch(rstStats());
    }

    return (
        <form className='settings-panel' onSubmit={e => setDeckAmount(e)}>
            <label className='input-checkbox settings-panel__input-checkbox' htmlFor='infinite'>
                <span>Infinite</span>
                <input onChange={() => toggleInfiniteAmount()} type='checkbox' className='input-checkbox__box' id='infinite' />
            </label>
            <input type='text' name='amount' className='input-text settings-panel__input-text' placeholder='666'
                disabled={reducer ? false : true} />
            <button type='submit'
                className='button settings-panel__button-refresh'
                disabled={reducer ? false : true}>
                <span>Refresh deck</span>
            </button>
            <button
                type='button'
                className='button settings-panel__button-reset'
                onClick={() => resetStats()}>
                <span>Reset stats</span>
            </button>
        </form>
    );
}

export default SettingsPanel;