import { useSelector } from "react-redux";
import './style.css';

function Stats() {

    const cards = useSelector(state => state.cards);

    function handleStatAnimation(e) {
        e.currentTarget.removeEventListener('animationend', handleStatAnimation);
    }

    return (
        cards.cards.length ?
            <div className="statistic">
                <h3 className="statistic__header">
                    Stats
                </h3>
                <ul className="statistic__list">
                    {cards.cards.length ?
                        <li className="statistic__list-item">
                            <span className="statistic__text statistic__text_uppercase">Picked</span>:
                            <span className="statistic__text statistic__text_bold">{cards.cards.length}</span>
                        </li> : null
                    }
                    {Object.keys(cards.stats).map(item => (
                        cards.stats[item] ?
                            <li key={item} className="statistic__list-item" onAnimationEnd={e => handleStatAnimation(e)}>
                                <span className="statistic__text">{item}</span>:
                                <span className="statistic__text statistic__text_bold">x{cards.stats[item]}</span>
                                ({Math.round(cards.stats[item] / cards.cards.length * 10000) / 100}%)
                            </li> : null
                    ))}
                </ul>
            </div> : null
    );
}

export default Stats;