import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { hideNotification } from '../store/reducers/notificationReducer';
import './style.css';
import sign from '../../asset/images/notification.png';

function Notification() {

    const dispatch = useDispatch();
    const notification = useSelector(state => state.notification);

    useEffect(() => {
        if (notification.status) {
            setTimeout(() => {
                dispatch(hideNotification());
            }, notification.timer);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [notification.timer]);

    function defineBackColor() {
        switch (notification.status) {
            case 'fine':
                return 'var(--color-notification-fine)';
            case 'err':
                return 'var(--color-notification-err)';
            default:
                return 'var(--color-shadow)';
        }
    }

    return (
        <div className='notification'
            style={{
                'transform': notification.status ? 'translateY(0%)' : 'translateY(100%)',
                'visibility': notification.status ? 'visible' : 'hidden',
                'backgroundColor': defineBackColor(),
            }}>
            <img className='notification__sign' src={sign} alt='sign' />
            <p className='notification__message'>{notification.message}</p>
        </div>
    );
}

export default Notification;