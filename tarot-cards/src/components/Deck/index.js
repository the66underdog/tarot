import { useSelector } from 'react-redux';
import Card from '../Card';

function Deck() {

    const amount = useSelector(state => state.cards.amount);

    return (
        <div className='deck'>
            <div className='deck__card deck__tip-container'>
                <span className='deck__tip'>Enter amount on the panel.<br />Press <br />"Refresh deck"</span>
            </div>
            {amount !== 0 && <Card />}
        </div>
    );
}

export default Deck;