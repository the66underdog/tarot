import { CardDescription } from "../Description";
import './style.css';

function Desc({ cardName, setDesc, isDesc }) {

    function toggleDescription(e) {
        e.relatedTarget instanceof Element &&
            !e.relatedTarget.closest('.moved') &&
            !e.relatedTarget.closest('.description') &&
            setDesc(false);
    }

    return (
        <aside className={`description${isDesc ? '' : ' description_hidden'}`}
            onMouseOut={e => toggleDescription(e)}>
            <p className="description__text">{CardDescription[cardName]}</p>
        </aside>
    );
}

export default Desc;